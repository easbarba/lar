;;; -*- lexical-binding: t;


;; =============== DEFAULT SETTINGS

;; ======================= CUSTOM GLOBAL VARIABLES

(defconst *site-lisp* (file-name-concat user-emacs-directory "site-lisp")
  "Emacs site-lisp folder.")

(defconst *home* (getenv "HOME")
  "$HOME folder location.")

(defconst *xdg-config* (file-name-concat *home* ".config")
  "XDG Config folder.")

(defconst *local* (file-name-concat *home* ".local")
  "$HOME/.local folder.")

(defconst *share* (file-name-concat *local* "share")
  "XDG local share folder.")

(defconst *download* (file-name-concat *home* "Downloads")
  "$HOME/Download folder.")

(defgroup *vars* nil
  "CUSTOM variables set by user"
  :group 'extensions
  :group 'convenience)

(defcustom *videos* (file-name-concat *home* "Videos")
  "Folder to save videos."
  :type 'string
  :group '*vars*)

(defcustom *music* (file-name-concat *home* "Music")
  "Folder to save songs."
  :type 'string
  :group '*vars*)

(defcustom *space* "?\s"
  "Folder to save songs."
  :type 'string
  :group '*vars*)

(defcustom *player* "mpv"
  "Default video/audio player."
  :type 'string
  :group '*vars*)

(defcustom *browser* "firefox"
  "Default Internet Browser."
  :type 'string
  :group '*vars*)

(defcustom *downloader* "wget"
  "Check if listed Downloaders exist, pick in order, else ask for one."
  :group '*vars*
  :type 'string)

(defcustom *media-downloader* "yt-dlp"
  "Youtube-dl - Media downloader."
  :type 'string)

(defcustom *theme* 'tango
  "Emacs default theme."
  :type 'string
  :group 'my)


(let ((framename (frame-parameter nil 'name)))
  (cond ((string= framename "leitura") (progn
					 (load-theme 'modus-vivendi-tritanopia t)
					 (dired (file-name-concat *home* "Documents" "livros"))))
	((string= framename "anotacao") (progn
					  (load-theme 'modus-operandi-deuteranopia t)
					  (dired (file-name-concat *home* "Documents" "anotacao"))))
	((string= framename "pessoal") (progn
					 (load-theme 'leuven-dark t)
					 (dired (file-name-concat *home* "Documents" "pessoal"))))
	((string= framename "midias") (progn
					(load-theme 'tsdh-dark t)
					(dired (file-name-concat *home* "Videos"))))
	((string= framename "cru") (progn
				     (load-theme 'manoj-dark t)
				     (dired (file-name-concat *home*))))
	((string= framename "term") (progn
				      (load-theme 'whiteboard t)
				      (dired (file-name-concat *home* ".local"))))
	(t (progn
	     (load-theme 'wombat t)
	     (dired (file-name-concat *home* "Documents" "trampo"))))))


(use-package erc
  :config
  (setq-local scroll-margin 1)
  :custom
  (erc-nick user-login-name
	    erc-server "irc.libera.chat"
	    erc-rename-buffers t
	    erc-kill-buffer-on-part t
	    erc-mode-line-format "%a"
	    erc-kill-queries-on-quit t
	    erc-kill-server-buffer-on-quit t
	    erc-query-display 'buffer
	    erc-save-buffer-on-part t
	    erc-log-channels-directory (concat user-emacs-directory "erc/logs")
	    erc-server-coding-system '(utf-8 . utf-8)
	    erc-track-enable-kebyindings nil
	    erc-prompt-for-nickserv-password nil
	    erc-hide-timestamps t
	    erc-join-buffer 'bury
	    erc-interpret-mirc-color t
	    erc-spelling-dictionaries '(("#emacs" "american"))
	    erc-autojoin-channels-alist '(("libera.chat"
					   "#c"
					   "#erlang"
					   "#linux"
					   "#gnu"
					   "#guile"
					   "#scheme"
					   "#emacs"))
	    erc-lurker-hide-list '("JOIN" "PART" "QUIT")
	    erc-track-exclude-types
	    '("JOIN" "NICK" "PART" "QUIT" "MODE"
	      "324" "329" "332" "333" "353" "477")))

(use-package org
  :init  (require 'ox-md)
  ;; :hook (org-mode . (lambda (setq truncate-lines t)))
  :custom
  (org-hide-leading-stars t
			  org-hide-emphasis-markers t
			  org-edit-src-content-indentation 0
			  org-src-tab-acts-natively t
			  org-confirm-babel-evaluate nil
			  org-support-shift-select 'always
			  org-src-fontify-natively t
			  org-fontify-whole-heading-line t
			  org-fontify-done-headline t
			  org-fontify-quote-and-verse-blocks t
			  org-log-done t
			  org-startup-with-inline-images nil
			  org-preview-latex-default-process 'dvisvgm
			  org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
  :config
  (defun e/tangle-on-save-org-mode-file()
    "Tangle org file on save."
    (interactive)
    (when (eq major-mode 'org-mode)
      (message "%s" major-mode)
      (org-babel-tangle)))
  (add-hook 'after-save-hook 'e/tangle-on-save-org-mode-file)

  (defun e/narrow-or-widen-dwim (p)
    "If the buffer is narrowed, it widens. Otherwise, it narrows intelligently.

      Intelligently means: region, org-src-block, org-subtree, or defun,
      whichever applies first.
      Narrowing to org-src-block actually calls `org-edit-src-code'.
      With prefix P, don't widen, just narrow even if buffer is already narrowed."
    (interactive "P")
    (declare (interactive-only))
    (cond ((and (buffer-narrowed-p) (not p)) (widen))
	  ((region-active-p)
	   (narrow-to-region (region-beginning) (region-end)))
	  ((derived-mode-p 'org-mode)
	   ;; `org-edit-src-code' is not a real narrowing command.
	   ;; Remove this first conditional if you don't want it.
	   (cond ((org-in-src-block-p)
		  (org-edit-src-code)
		  (delete-other-windows))
		 ((org-at-block-p)
		  (org-narrow-to-block))
		 (t (org-narrow-to-subtree))))
	  (t (narrow-to-defun))))
  (global-set-key (kbd "C-c d") 'e/narrow-or-widen-dwim)

  (eval-after-load 'org-src
    '(define-key org-src-mode-map
		 "\C-x\C-s" #'org-edit-src-exit)))

(use-package image
  :hook (image-mode .
		    (lambda ()
		      (local-set-key
		       (kbd "<right>")

		       (lambda ()
			 (interactive)
			 (image-next-file 1)
			 (image-transform-fit-to-width)))
		      (local-set-key
		       (kbd "<left>")

		       (lambda ()
			 (interactive)
			 (image-previous-file 1)
			 (image-transform-fit-to-width)))))
  :custom (image-use-external-converter t))

(use-package ido
  :init
  (ido-mode 1)
  :custom
  (ido-everywhere t)
  (ido-enable-flex-matching t)
  (ido-create-new-buffer 'always)
  (ido-enable-prefix nil)
  (ido-enable-regexp t)
  (ido-decorations (quote ("\n-> " "" "\n " "\n ..." "[" "]" " [No match]" " [Matched]" " [Not readable]" " [Too big]" " [Confirm]")))
  (ido-use-filename-at-point 'guess)
  (ido-file-extensions-order '(".lisp" ".org" ".el" ".scm" ".ts" ".py"))
  (ido-max-directory-size 100000)
  (ido-use-url-at-point t)
  (ido-enable-dot-prefix t)
  (ido-use-virtual-buffers t)
  :config
  (defun e/ido-bookmark-jump (bname)
    "Switch to bookmark BNAME interactively using `ido'."
    (interactive
     (list (ido-completing-read "Bookmark: " (bookmark-all-names) nil t)))
    (bookmark-jump bname))

  (defun e/recentf-ido-find-file ()
    "Find a recent file using Ido."
    (interactive)
    (let ((file (ido-completing-read "Choose recent file: " recentf-list nil t)))
      (when file
	(find-file file))))

  (defun e/ido-load-theme ()
    "Load theme using Ido."
    (interactive)
    (let* ((themes (mapcar (lambda (x) (symbol-name x)) (custom-available-themes)))
	   (theme (ido-completing-read "Choose recent file: " themes nil t)))
      (when theme
	(load-theme (intern theme) nil))))

  (defun e/mx-ido ()
    "Open Mx in ido-fashioned way."
    (interactive)
    (call-interactively
     (intern
      (ido-completing-read
       "M-x "
       (all-completions "" obarray 'commandp)))))

  ;; (:map ido-completion-map
  ;;	("C-n" . 'ido-next-match)
  ;;	("C-p" . 'ido-prev-match))

  (define-key (cdr ido-minor-mode-map-entry) [remap write-file] nil)
  :bind (("C-c r" . 'e/recentf-ido-find-file)
	 ("C-x C-f" . 'ido-find-file)))

(use-package eshell
  :init
  (require 'em-smart)
  (require 'em-term)
  :custom
  (eshell-scroll-to-bottom-on-input 'all)
  (eshell-visual-subcommands '("git" "commit" "--amend" "log" "l" "diff" "show"))
  (eshell-error-if-no-glob t)
  (eshell-where-to-jump 'begin)
  (eshell-review-quick-commands nil)
  (eshell-smart-space-goes-to-end t)
  (eshell-hist-ignoredups t)
  (eshell-save-history-on-exit t)
  (eshell-prefer-lisp-functions nil)
  (eshell-destroy-buffer-when-process-dies t)
  :config
  (let ((apps '(ssh tail htop emacs vim)))
    (dolist (app apps)
      (add-to-list 'eshell-visual-commands (format "%s" app))))

  (setenv "PATH" (concat "/usr/local/bin:/usr/local/sbin:" (getenv "PATH")))
  (getenv "PATH")
  (setenv "PAGER" "cat")

  (add-hook 'eshell-expand-input-functions #'eshell-expand-history-references)

  (defun eshell/find-file (file)
    "Find FILE using Eshell."
    (find-file file))

  (defun eshell/clear ()
    "Function to clear eshell buffer."
    (let ((eshell-buffer-maximum-lines 0))
      (eshell-truncate-buffer)))

  (defun eshell/gst (&rest args)
    "Git status ARGS."
    (magit-status (pop args) nil)
    (eshell/echo))   ;; The echo command suppresses output

  (defun eshell/find (&rest args)
    "Wrapper around the ‘find’ executable using ARGS."
    (let ((cmd (concat "find " (string-join args))))
      (shell-command-to-string cmd)))

  (defun eshell/hp (&rest args)
    "Emily run ARGS."
    (let ((cmd (concat "cero" *space* (string-join args))))
      (shell-command-to-string cmd)))

  (defun eshell/dp (&rest args)
    "Emily - run distro ARGS."
    (let ((cmd (concat "distro" *space* (string-join args))))
      (shell-command-to-string cmd)))

  (defun e/eshell-here ()
    "Open Eshell using directory associated with the current buffer's file.

      The eshell is renamed to match that
      directory to make multiple eshell windows easier."
    (interactive)
    (let* ((height (/ (window-total-height) 3)))
      (split-window-vertically (- height))
      (other-window 1)
      (eshell "new")
      (insert (concat "ls"))
      (eshell-send-input)))
  (global-set-key (kbd "C-c E") 'e/eshell-here)

  (defun e/eshell-quit-or-delete-char (arg)
    "ARG."
    (interactive "p")
    (if (and (eolp) (looking-back eshell-prompt-regexp))
	(progn
	  (eshell-life-is-too-much) ; Why not? (eshell/exit)
	  (ignore-errors
	    (delete-window)))
      (delete-forward-char arg)))

  (add-hook 'eshell-mode-hook
	    (lambda ()
	      (global-set-key (kbd "C-d") 'e/eshell-quit-or-delete-char)))

  (defun e/eshell-there (host)
    "Eshell with Tramp automatically connect to a remote system, HOST.

      The hostname can be either the IP address, or FQDN,
      and can specify the user account, as in
      root@blah.com. HOST can also be a complete Tramp reference."
    (interactive "sHost: ")

    (let* ((default-directory
	    (cond
	     ((string-match-p "^/" host) host)

	     ((string-match-p (ha/eshell-host-regexp 'full) host)
	      (string-match (ha/eshell-host-regexp 'full) host)
	      (let* ((user1 (match-string 2 host))
		     (host1 (match-string 3 host))
		     (user2 (match-string 6 host))
		     (host2 (match-string 7 host)))
		(if host1
		    (ha/eshell-host->tramp user1 host1)
		  (ha/eshell-host->tramp user2 host2))))

	     (t (format "/%s:" host)))))
      (eshell-here)))

  (defun e/eshell-close ()
    "Close eshell."
    (insert "exit")
    (eshell-send-input)
    (delete-window))

  (defun eshell-next-prompt (n)
    "Move to end of Nth next prompt in the buffer.
  See `eshell-prompt-regexp'."
    (interactive "p")
    (re-search-forward eshell-prompt-regexp nil t n)
    (when eshell-highlight-prompt
      (while (not (get-text-property (line-beginning-position) 'read-only) )
	(re-search-forward eshell-prompt-regexp nil t n)))
    (eshell-skip-prompt))

  (defun eshell-previous-prompt (n)
    "Move to end of Nth previous prompt in the buffer.
  See `eshell-prompt-regexp'."
    (interactive "p")
    (backward-char)
    (eshell-next-prompt (- n)))

  (defun eshell-insert-history ()
    "Displays the eshell history to select and insert back into your eshell."
    (interactive)
    (insert (ido-completing-read "Eshell history: "
				 (delete-dups
				  (ring-elements eshell-history-ring)))))

  (add-hook 'eshell-mode-hook
	    (lambda ()
	      (define-key eshell-mode-map (kbd "M-S-P") 'eshell-previous-prompt)
	      (define-key eshell-mode-map (kbd "M-S-N") 'eshell-next-prompt)
	      (define-key eshell-mode-map (kbd "M-r") 'eshell-insert-history)))

  (add-hook 'after-save-hook
	    'executable-make-buffer-file-executable-if-script-p)

  (defun e/shell-here ()
    "Open `shell' using directory associated with the current buffer's file.

	 The `shell' is renamed to match that
	 directory to make multiple eshell windows easier."
    (interactive)
    (let* ((height (/ (window-total-height) 3)))
      (split-window-vertically (- height))
      (other-window 1)
      (shell))))

(use-package ansi-term
  :disabled
  :bind ("C-c T" . #'e/ansi-term-here)
  :config
  (defun e/ansi-term-here ()
    "Open `ansi-term' using directory associated with the current buffer's file.

      The `ansi-term' is renamed to match that
      directory to make multiple eshell windows easier."
    (interactive)
    (let* ((height (/ (window-total-height) 3)))
      (split-window-vertically (- height))
      (other-window 1)
      (ansi-term "bash" "Mini-Shell"))))

(use-package flymake
  :bind (("C-c b f" . flymake-show-diagnostics-buffer)
	 ("M-n"     . flymake-goto-next-error)
	 ("M-p"     . flymake-goto-prev-error)))

(use-package info
  :init
  :custom (Info-additional-directory-list
	   (list (expand-file-name (concat *share* "/info/")))))

(use-package vc
  :init (define-key vc-prefix-map "=" 'vc-ediff)
  :custom (vc-dir-backend 'git vc-make-backup-files t))

(use-package diff
  :init
  (setq-local whitespace-style
	      '(face
		tabs
		tab-mark
		spaces
		space-mark
		trailing
		indentation::space
		indentation::tab
		newline
		newline-mark))
  (whitespace-mode 1))

(use-package ediff
  :init
  (setq-default ediff-diff-options "-w"
		ediff-split-window-function 'split-window-horizontally
		ediff-window-setup-function 'ediff-setup-windows-plain)
  (add-hook 'ediff-after-quit-hook-internal 'winner-undo))

(use-package outline
  :init (outline-minor-mode)
  :custom (outline-regexp ";;\\ \\*"))

(use-package recentf
  :init
  (recentf-mode 1)
  (run-at-time nil (* 5 120) 'recentf-save-list)
  :custom
  (recentf-max-saved-items 1000)
  (recentf-max-menu-items 60) ;; disable recentf-cleanup on Emacs start, because it can cause
  (recentf-auto-cleanup 'never) ;; problems with remote files
  (recentf-auto-cleanup 600)  ;; clean up the recent files
  (recentf-exclude '("^/var/folders\\.*"
		     "COMMIT_MSG"
		     "[0-9a-f]\\{32\\}-[0-9a-f]\\{32\\}\\.org"
		     "github.*txt$"
		     "COMMIT_EDITMSG\\'"
		     ()			".*-autoloads\\.el\\'"
		     "recentf"
		     ".*pang$" ".*cache$"
		     "[/\\]\\.elpa/")) ;; exclude ** from recentfiles buffer

  :config
  ;; Add visited dired visited directories to recentf
  (defun recentf-track-opened-file ()
    "Insert dired/file name just opened or written into the recent list."
    (let ((buff-name (or buffer-file-name
			 (and (derived-mode-p 'dired-mode)
			      default-directory))))
      (and buff-name
	   (recentf-add-file buff-name)))
    ;; Must return nil because it is run from `write-file-functions'.
    nil)

  (defun recentf-track-closed-file ()
    "Update the recent list when a file or dired buffer is killed.
That is, remove a non kept file from the recent list."
    (let ((buff-name (or buffer-file-name
			 (and
			  (derived-mode-p 'dired-mode)
			  default-directory))))
      (and buff-name
	   (recentf-remove-if-non-kept buff-name))))
  (add-hook 'dired-after-readin-hook 'recentf-track-opened-file))

(use-package ibuffer
  :init
  (global-set-key (kbd "C-c b i") 'ibuffer)
  (defalias 'list-buffers 'ibuffer)) ;; make ibuffer the default buffer manager.

(use-package doc-view
  :config
  (setq doc-view-continuous t
	doc-view-resolution 400)

  (add-hook 'auto-revert-mode 'doc-view)
  (define-key doc-view-mode-map (kbd "/") #'doc-view-reverse-colors)

  (defun define-doc-view-current-cache-dir ()
    "`doc-view-current-cache-dir' was renamed to function `doc-view--current-cache-dir' in Emacs 24.5."
    (or (fboundp 'doc-view-current-cache-dir)
	(defalias 'doc-view-current-cache-dir 'doc-view--current-cache-dir)))
  (eval-after-load "doc-view" '(define-doc-view-current-cache-dir))

  (defun doc-view-reverse-colors ()
    "Inverts document colors.
Requires an installation of ImageMagick (\"convert\")."
    (interactive)
    ;; error out when ImageMagick is not installed
    (if (/= 0 (call-process-shell-command "convert -version"))
	(error "Reverse colors requires ImageMagick (convert)")
      (when (eq major-mode 'doc-view-mode)
	;; assume current doc-view internals about cache-names
	(let ((file-name (expand-file-name (format "page-%d.png"
						   (doc-view-current-page))
					   (doc-view-current-cache-dir))))
	  (call-process-shell-command
	   "convert" "-negate" file-name file-name)
	  (clear-image-cache)
	  (doc-view-goto-page (doc-view-current-page))))))

  (defun doc-view-reverse-colors-all-pages ()
    "Inverts document colors on all pages.
Requires an installation of ImageMagick (\"convert\")."
    (interactive)
    ;; error out when ImageMagick is not installed
    (if (/= 0 (call-process-shell-command "convert -version"))
	(error "Reverse colors requires ImageMagick (convert)")
      (when (eq major-mode 'doc-view-mode)
	;; assume current doc-view internals about cache-names
	(let ((orig (doc-view-current-page))
	      (page nil))
	  (message "Reversing video on all pages...")
	  (dotimes (pnum (doc-view-last-page-number))
	    (setq page (expand-file-name (format "page-%d.png" (1+ pnum))
					 (doc-view-current-cache-dir)))
	    (call-process-shell-command
	     "convert" "-negate" page page))
	  (clear-image-cache)
	  (doc-view-goto-page orig)
	  (message "Done reversing video!")))))

  (easy-menu-define e/doc-view-menu doc-view-mode-map "Menu for Doc-View Mode."
    '("DocView"
      ["Switch to a different mode" doc-view-toggle-display
       :help "Switch to a different mode"]
      ["Open Text" doc-view-open-text
       :help "Display the current doc's contents as text"]
      "--"
      ("Navigate Doc"
       ["Goto Page ..." doc-view-goto-page
	:help "View the page given by PAGE"]
       "--"
       ["Scroll Down" doc-view-scroll-down-or-previous-page
	:help "Scroll page down ARG lines if possible, else goto previous page"]
       ["Scroll Up" doc-view-scroll-up-or-next-page
	:help "Scroll page up ARG lines if possible, else goto next page"]
       "--"
       ["Next Line" doc-view-next-line-or-next-page
	:help "Scroll upward by ARG lines if possible, else goto next page"]
       ["Previous Line" doc-view-previous-line-or-previous-page
	:help "Scroll downward by ARG lines if possible, else goto previous page"]
       ("Customize"
	["Continuous Off"
	 (setq doc-view-continuous nil)
	 :help "Stay put in the current page, when moving past first/last line"
	 :style radio :selected
	 (eq doc-view-continuous nil)]
	["Continuous On"
	 (setq doc-view-continuous t)
	 :help "Goto to the previous/next page, when moving past first/last line"
	 :style radio :selected
	 (eq doc-view-continuous t)]
	"---"
	["Save as Default"
	 (customize-save-variable 'doc-view-continuous doc-view-continuous)
	 t])
       "--"
       ["Next Page" doc-view-next-page :help "Browse ARG pages forward"]
       ["Previous Page" doc-view-previous-page :help "Browse ARG pages backward"]
       "--"
       ["First Page" doc-view-first-page :help "View the first page"]
       ["Last Page" doc-view-last-page :help "View the last page"])
      "--"
      ("Adjust Display"
       ["Enlarge" doc-view-enlarge :help "Enlarge the document by FACTOR"]
       ["Shrink" doc-view-shrink :help "Shrink the document"]
       "--"
       ["Fit Width To Window" doc-view-fit-width-to-window
	:help "Fit the image width to the window width"]
       ["Fit Height To Window" doc-view-fit-height-to-window
	:help "Fit the image height to the window height"]
       "--"
       ["Fit Page To Window" doc-view-fit-page-to-window
	:help "Fit the image to the window"]
       "--"
       ["Set Slice From Bounding Box" doc-view-set-slice-from-bounding-box
	:help "Set the slice from the document's BoundingBox information"]
       ["Set Slice Using Mouse" doc-view-set-slice-using-mouse
	:help "Set the slice of the images that should be displayed"]
       ["Set Slice" doc-view-set-slice
	:help "Set the slice of the images that should be displayed"]
       ["Reset Slice" doc-view-reset-slice
	:help "Reset the current slice"])
      ("Search"
       ["New Search ..."
	(doc-view-search t)
	:help
	"Jump to the next match or initiate a new search if NEW-QUERY is given"]
       "--"
       ["Search" doc-view-search
	:help
	"Jump to the next match or initiate a new search if NEW-QUERY is given"]
       ["Backward" doc-view-search-backward
	:help "Call `doc-view-search' for backward search"]
       "--"
       ["Show Tooltip" doc-view-show-tooltip
	:help nil])
      ("Maintain"
       ["Reconvert Doc" doc-view-reconvert-doc
	:help "Reconvert the current document"]
       "--"
       ["Clear Cache" doc-view-clear-cache
	:help "Delete the whole cache (`doc-view-cache-directory')"]
       ["Dired Cache" doc-view-dired-cache
	:help "Open `dired' in `doc-view-cache-directory'"]
       "--"
       ["Revert Buffer" doc-view-revert-buffer
	:help "Like `revert-buffer', but preserves the buffer's current modes"]
       "--"
       ["Kill Proc" doc-view-kill-proc
	:help "Kill the current converter process(es)"]
       ["Kill Proc And Buffer" doc-view-kill-proc-and-buffer
	:help "Kill the current buffer"])
      "--"
      ["Customize"
       (customize-group 'doc-view)]))
  (easy-menu-define e/doc-view-minor-mode-menu doc-view-minor-mode-map
    "Menu for Doc-View Minor Mode."
    '("DocView*"
      ["Display in DocView Mode" doc-view-toggle-display
       :help "View"]
      ["Exit DocView Mode" doc-view-minor-mode])))

(use-package flyspell
  :config
  (if (executable-find "aspell")
      (progn
	(setq ispell-program-name "aspell"
	      ispell-extra-args '("--sug-mode=ultra")))
    (setq ispell-program-name "ispell")))

(use-package icomplete
  :init (icomplete-mode 1))

(use-package abbrev
  :config
  (defun xah-global-abbrev-position-cursor (&optional @pos)
    "Move cursor back to ^ if exist, else put at end.
Return true if found, else false. Version 2016-10-24"
    (interactive)
    (let (($found-p (search-backward "^" (if @pos
					     @pos
					   (max (point-min) (- (point) 100)))
				     t)))
      (when $found-p (delete-char 1))
      $found-p)))

;; (advice-add 'expand-abbrev :after (lambda () (xah-global-abbrev-position-cursor)))
;; ;; mimics yasnippet point move to  !
;; (advice-add 'expand-abbrev :after (lambda () (move-to-point activate)
;;				    (xah-global-abbrev-position-cursor)))

(use-package saveplace
  :init
  (save-place-mode 1)
  (setq-default save-place t)
  :custom
  (save-place-file (expand-file-name "etc/places" user-emacs-directory))
  (backup-directory-alist `(("." . ,(concat user-emacs-directory
					    "etc/backups")))))

(use-package goto-addr
  :config
  (add-hook 'prog-mode-hook 'goto-address-mode)
  (add-hook 'text-mode-hook 'goto-address-mode))

(use-package hippie-exp
  :init
  (global-set-key (kbd "<C-tab>") 'hippie-expand)
  :custom
  (hippie-expand-try-functions-list
   '(yas-hippie-try-expand
     ;; Try to expand word "dynamically", searching the current buffer.
     try-expand-dabbrev
     ;; Try to expand word "dynamically", searching all other buffers.
     try-expand-dabbrev-all-buffers
     ;; Try to expand word "dynamically", searching the kill ring.
     try-expand-dabbrev-from-kill
     ;; Try to complete text as a file name, as many characters as unique.
     try-complete-file-name-partially
     ;; Try to complete text as a file name.
     try-complete-file-name
     ;; Try to complete as an Emacs Lisp symbol, as many characters as unique.
     try-complete-lisp-symbol-partially
     ;; Try to complete word as an Emacs Lisp symbol.
     try-complete-lisp-symbol
     ;; Try to expand word before point according to all abbrev tables.
     try-expand-all-abbrevs
     ;; Try to complete the current line to an entire line in the buffer.
     try-expand-list
     ;; Try to complete the current line to an entire line in the buffer.
     try-expand-line)))

(use-package dabbrev
  :defer t
  :bind (("M-/" . dabbrev-completion)   ;; Swap M-/ and C-M-/
	 ("C-M-/" . dabbrev-expand))
  :config
  (add-to-list 'dabbrev-ignored-buffer-regexps "\\` ")
  ;; Since 29.1, use `dabbrev-ignored-buffer-regexps' on older.
  (add-to-list 'dabbrev-ignored-buffer-modes 'doc-view-mode)
  (add-to-list 'dabbrev-ignored-buffer-modes 'pdf-view-mode)
  (add-to-list 'dabbrev-ignored-buffer-modes 'tags-table-mode))

(use-package whitespace
  :init
  (add-hook 'before-save-hook 'whitespace-cleanup)
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (add-hook 'prog-mode-hook
	    (lambda () (interactive) (setq show-trailing-whitespace 1)))
  (add-hook 'org-mode-hook
	    (lambda () (interactive) (setq show-trailing-whitespace 1)))

  ;; (add-hook 'before-save-hook 'delete-trailing-whitespace)
  :custom
  (show-trailing-whitespace t)
  (whitespace-style '(face lines-tail)))

(use-package savehist
  :init
  (savehist-mode t)
  (setq savehist-save-minibuffer-history t
	savehist-autosave-interval nil
	;; savehist-file (expand-file-name "etc/savehist" user-emacs-directory)
	savehist-save-minibuffer-history 1
	savehist-additional-variables '(Info-history-list)
	savehist-additional-variables '(kill-ring search-ring regexp-search-ring)))

(use-package eww
  :bind (:map eww-mode-map ("W" . 'define-word-at-point)))

(use-package windmove
  :custom (windmove-wrap-around t))

(use-package emacs
  :custom (prettify-symbols-alist '(("lambda" . 955)))
  :config
  (global-prettify-symbols-mode 1)
  (defun e/add-pretty-lambda ()
    "Lisp symbols as pretty Unicode symbols."
    (setq prettify-symbols-alist
	  '(("lambda" . 955)  ;; λ
	    ("->" . 8594)     ;; →
	    ("=>" . 8658)     ;; ⇒
	    ("map" . 8614)))) ;; ↦
  :hook ((scheme-mode clojure-mode haskell-mode) . 'e/add-pretty-lambda))

(use-package winner
  :init (winner-mode 1))

(use-package midnight
  :init (midnight-delay-set 'midnight-delay "4:30am")
  :custom (midnight-period 5000))

(use-package uniquify
  :custom ((uniquify-buffer-name-style 'forward)
	   (uniquify-after-kill-buffer-p t)
	   (uniquify-separator "/")
	   (uniquify-ignore-buffers-re "^\\*")))

(use-package isearch
  :config
  (defun e/isearch-exit-other-end ()
    "Exit isearch, at the opposite end of the string."
    (interactive)
    (isearch-exit)
    (goto-char isearch-other-end))
  (define-key isearch-mode-map [(control return)] #'isearch-exit-other-end))

(use-package eldoc
  :custom
  (eldoc-idle-delay 0.1)
  (eldoc-echo-area-use-multiline-p nil))

(use-package paren
  :init (show-paren-mode 1))

(use-package dired
  :hook (dired-mode . (lambda ()
			(require 'wdired)
			(setq wdired-allow-to-change-permissions t
			      wdired-allow-to-redirect-links t)

			(require 'dired-x)
			(put 'dired-find-alternate-file 'disabled nil)
			(dired-hide-details-mode)))
  :custom
  (global-auto-revert-non-file-buffers t)
  (dired-omit-mode t)
  (dired-dwim-target t)
  (dired-recursive-deletes (quote top))
  (dired-recursive-deletes t)
  (dired-compress-files-alist
   '(("\\.zip\\'" . "zip %o -r --filesync %i")
     ("\\.tar\\.gz\\'" . "tar -c %i | gzip -c9 > %o")))
  (dired-guess-shell-alist-user
   (list
    '("\\.odt\\'" "libreoffice")
    '("\\.\\(?:mp4\\|webm\\|mkv\\|ogv\\)\\(?:\\.part\\)?\\'"
      ,*player*)
    '("\\.html?\\'" *browser*)))
  (dired-create-destination-dirs t)
  (auto-revert-verbose nil)
  (dired-listing-switches "-laGh1v --group-directories-first")
  :config
  ;; (require 'dired)

  ;; (when (eq system-type 'berkeley-unix)
  ;;   (setq insert-directory-program "gls"))
  (defun e/dired-up-directory ()
    "Ora - Up directory no spawn directories in Ibuffer."
    (interactive)
    (let ((this-directory default-directory)
	  (buffer (current-buffer)))
      (dired-up-directory)
      (unless (cl-find-if
	       (lambda (w)
		 (with-selected-window w
		   (and (eq major-mode 'dired-mode)
			(equal default-directory this-directory))))
	       (delete (selected-window) (window-list)))
	(kill-buffer buffer))))

  (defun e/dired-open-file ()
    "In dired, open the file named on this line."
    (interactive)
    (let* ((file (dired-get-filename nil t)))
      (message "Opening %s..." file)
      (call-process "xdg-open" nil 0 nil file)
      (message "Opening %s done" file)))

  (define-key dired-mode-map "z" 'dired-do-compress)
  (define-key dired-mode-map "^"
	      (lambda () (interactive) (find-alternate-file "..")))
  (define-key dired-mode-map "b" 'e/dired-up-directory)
  (define-key dired-mode-map "r" 'e/dired-open-file)
  (define-key dired-mode-map (kbd "C-c j") 'dired-two-panel)
  (define-key dired-mode-map "I" (lambda () (interactive) (info (dired-get-filename)))))

(use-package which-key
  :config (which-key-mode))

;; ============================
;; * CUSTOM FUNCTIONS
;; ============================

(use-package emacs
  :config
  (defun e/search-engine ()
    "Search term on internet search engines/repositories.
     By default use word-at-point or ask for a term."
    (interactive)
    (let* ((word-at-point (word-at-point))
	   (symbol-at-point (symbol-at-point))
	   (term-at-point (symbol-name symbol-at-point))
	   (term-buffer (read-from-minibuffer ;; Ask for a term to be entered.
			 (if (or word-at-point symbol-at-point)
			     (concat
			      "Symbol (default "
			      term-at-point
			      "): ") "Search for: (no default): "))))

      ;; use term-at-point or term-buffer
      (let ((term (if (string= term-buffer "")
		      term-at-point
		    term-buffer))
	    ;; Pick a search engine
	    (search-type
	     (read-from-minibuffer
	      "Search on SoV, DDG? ")))
	(cond
	 ((string-equal search-type "s")
	  (browse-url (concat "https://stackoverflow.com/search?q=" term)))
	 ((string-equal search-type "d")
	  (browse-url (concat "https://duckduckgo.com/?q=" term)))))))
  (global-set-key (kbd "C-c f s") 'e/search-engine)

  (defun e/play-video (url)
    "Call Video Player with online video's URL on clipboard!"
    (interactive)
    (alert "Playing Video.")
    (start-process "PLAY-VIDEO" "PLAY-VIDEO" *player* url))

  (defun e/dired-count-marked-files ()
    "Count marked files."
    (interactive)
    (let ((count 0))
      (dolist (file (dired-get-marked-files))
	(setq count (1+ count))
	(message "%s" count))))

  (defun e/kill-this-buffer-for-real ()
    "Kill the current buffer and window."
    (interactive)
    (kill-this-buffer)
    (delete-window))
  (global-set-key (kbd "C-c k") 'e/kill-this-buffer-for-real)

  (defun e/filepath-to-clipboard ()
    (interactive)
    (kill-new (buffer-file-name)))
  (global-set-key (kbd "C-c b q") 'e/filepath-to-clipboard)

  (defun e/yt-feed()
    (interactive)
    (let* ((base "https://www.youtube.com/feeds/videos.xml?channel_id=")
	   (url (current-kill 0 t))
	   (id (car (last (split-string url "/"))))
	   (name (read-from-minibuffer "Channel name: ")))
      (newline-and-indent)
      (previous-line 1)
      (insert (concat "\"" base id "\" ;; ") name)))

  (defun e/sudo-edit (&optional arg)
    "Edit currently visited file as root.

     With a prefix ARG prompt for a file to visit.
     Will also prompt for a file to visit if current
     buffer is not visiting a file."
    (interactive "P")
    (if (or arg (not buffer-file-name))
	(find-file (concat "/sudo:root@localhost:"
			   (ido-read-file-name "Find file(as root): ")))
      (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

  (defun e/copy-line ()
    "Copy entire line - aboabo."
    (interactive)
    (save-excursion
      (back-to-indentation)
      (kill-ring-save
       (point)
       (line-end-position)))
    (message "1 line copied"))
  (global-set-key (kbd "C-c a w") 'e/copy-line)

  (defun e/change-theme (&rest args)
    "Like `load-theme', but disable all themes before loading the new one, ARGS."
    ;; The `interactive' magic is for creating a future-proof passthrough.
    (interactive (advice-eval-interactive-spec
		  (cadr (interactive-form #'load-theme))))
    (mapc #'disable-theme custom-enabled-themes)
    (apply (if (called-interactively-p 'any) #'funcall-interactively #'funcall)
	   #'load-theme args))


  (defun e/toggle-window-split ()
    (interactive)
    (if (= (count-windows) 2)
	(let* ((this-win-buffer (window-buffer))
	       (next-win-buffer (window-buffer (next-window)))
	       (this-win-edges (window-edges (selected-window)))
	       (next-win-edges (window-edges (next-window)))
	       (this-win-2nd (not (and (<= (car this-win-edges)
					   (car next-win-edges))
				       (<= (cadr this-win-edges)
					   (cadr next-win-edges)))))
	       (splitter
		(if (= (car this-win-edges)
		       (car (window-edges (next-window))))
		    'split-window-horizontally
		  'split-window-vertically)))
	  (delete-other-windows)
	  (let ((first-win (selected-window)))
	    (funcall splitter)
	    (if this-win-2nd (other-window 1))
	    (set-window-buffer (selected-window) this-win-buffer)
	    (set-window-buffer (next-window) next-win-buffer)
	    (select-window first-win)
	    (if this-win-2nd (other-window 1))))))

  (define-key ctl-x-4-map "t" 'e/toggle-window-split)

  (defun e/emacs-packages-count ()
    (interactive)
    (message "%s package(s) loaded in %s with %s gc."
	     (length package-activated-list) (emacs-init-time) gcs-done)))

;; =================
;; LANGUAGES
;; =================
(use-package eglot
  :custom (eglot-autoshutdown t)
  :bind (("C-c a r" . eglot-rename)
	 ("C-c a a" . eglot-code-actions)
	 ("C-c a f" . eglot-format)
	 ("C-c a h" . eldoc)
	 ("C-c a h" . eglot-help-at-point)
	 ("C-."     . xref-find-definitions)))

(use-package treesit
  :config
  (setq treesit-language-source-alist
	'((html "https://github.com/tree-sitter/tree-sitter-html" "master" "src")
	  (css "https://github.com/tree-sitter/tree-sitter-css" "master" "src")
	  (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
	  (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
	  (json "https://github.com/tree-sitter/tree-sitter-json" "master" "src")
	  (markdown "https://github.com/ikatyang/tree-sitter-markdown" "master" "src")
	  (python "https://github.com/tree-sitter/tree-sitter-python" "master" "src")
	  (clojure "https://github.com/sogaiu/tree-sitter-clojure" "master" "src")
	  (csharp "https://github.com/tree-sitter/tree-sitter-c-sharp" "master" "src")
	  (haskell "https://github.com/tree-sitter/tree-sitter-haskell" "master" "src")
	  (zig "https://github.com/GrayJack/tree-sitter-zig" "master" "src")
	  (elixir "https://github.com/elixir-lang/tree-sitter-elixir" "main" "src")
	  (heex "https://github.com/phoenixframework/tree-sitter-heex" "main" "src")
	  (java "https://github.com/tree-sitter/tree-sitter-java" "master" "src")
	  (php "https://github.com/tree-sitter/tree-sitter-php" "master" "php/src")
	  (phpdoc "https://github.com/claytonrcarter/tree-sitter-phpdoc" "master" "php/src")
	  (bash "https://github.com/tree-sitter/tree-sitter-bash" "master" "src")
	  (go "https://github.com/tree-sitter/tree-sitter-go" "master" "src")
	  (ruby "https://github.com/tree-sitter/tree-sitter-ruby" "master" "src")
	  (rust "https://github.com/tree-sitter/tree-sitter-rust" "master" "src")
	  (cpp "https://github.com/tree-sitter/tree-sitter-cpp" "master" "src")
	  (c "https://github.com/tree-sitter/tree-sitter-c" "master" "src")))

  (let ((tsdir (file-name-concat user-emacs-directory "tree-sitter")))
    (dolist (langinfo treesit-language-source-alist)
      (let* ((lang (car langinfo))
	     (langpath (file-name-concat tsdir (concat "libtree-sitter-" (symbol-name lang) ".so"))))
	(unless (file-exists-p langpath)
	  (treesit-install-language-grammar lang)))))
  :mode (("\\.yaml\\'" . yaml-mode)
	 ("\\.yml\\'" . yaml-mode)
	 ("\\.js\\'" . javascript-ts-mode)
	 ("\\.ts\\'" . typescript-ts-mode)
	 ("\\.bash\\'" . bash-ts-mode)
	 ("\\.sh\\'" . bash-ts-mode)
	 ;; ("\\.swift\\'" . swift-mode)
	 ("\\.css\\'" . css-ts-mode)
	 ("\\.html\\'" . html-ts-mode)
	 ("\\.py\\'" . python-ts-mode)
	 ("\\.ex\\'" . elixir-ts-mode)
	 ("\\.eex\\'" . elixir-ts-mode)
	 ("\\.exs\\'" . elixir-ts-mode)
	 ("\\.php\\'" . php-ts-mode)
	 ("\\.go\\'" . go-ts-mode)
	 ("\\.java\\'" . java-ts-mode)
	 ("\\.c\\'" . c-ts-mode)
	 ("\\.h\\'" . c-ts-mode))
  :hook ((javascript-ts-mode
	  css-ts-mode
	  html-ts-mode
	  json-ts-mode
	  python-ts-mode
	  php-ts-mode
	  go-ts-mode
	  ;; swift-mode
	  bash-ts-mode bash-mode sh-mode
	  java-ts-mode
	  csharp-mode
	  cc-mode c-mode-hook c-ts-mode) . eglot-ensure)
  :config
  (with-eval-after-load 'eglot
    (add-to-list 'eglot-server-programs
		 '(elixir-ts-mode . ("elixir-ls")))
    (add-to-list 'eglot-server-programs
		 '(php-mode . ("phpactor" "language-server"))))
  :defer t)

;; =============== REPOSITORIES

(unless (string= (frame-parameter nil 'name) "cru")
  (when (executable-find "git")
    (require 'package)
    (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
    (package-initialize)

    (add-to-list 'load-path (file-name-concat
			     user-emacs-directory
			     "lisp"))
    (let ((pkgs '("nongnu-libs.el"
		  "nongnu-builtin.el"
		  "nongnu-prog.el"
		  "nongnu-tools.el"
		  "nongnu-ui.el")))
      (dolist (p pkgs)
	(load-library p)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages nil)
 '(package-vc-selected-packages
   '((corfu-popupinfo :url "https://github.com/minad/corfu" :lisp-dir
		      "extensions")
     (counsel :url "https://github.com/abo-abo/swiper" :make "compile")
     (swiper :url "https://github.com/abo-abo/swiper" :make "compile")
     (perspective :url "https://github.com/nex3/perspective-el" :make
		  "compile")
     (highlight-indent-guides :url
			      "https://github.com/DarthFennec/highlight-indent-guides"
			      :make "lisp")
     (deft :url "https://github.com/jrblevin/deft" :make "all")
     (diff-hl :url "https://github.com/dgutov/diff-hl" :make "compile")
     (hl-todo :url "https://github.com/tarsius/hl-todo" :make "lisp")
     (pdf-tools :url "https://github.com/vedang/pdf-tools" :lisp-dir
		"lisp" :shell-command "cask install")
     (yeetube :url "https://codeberg.org/ThanosApollo/emacs-yeetube")
     (browse-at-remote :url
		       "https://github.com/rmuslimov/browse-at-remote"
		       :make "compile")
     (markdown :url "https://github.com/jrblevin/markdown-mode" :make
	       "all")
     (geiser :url "https://gitlab.com/emacs-geiser/geiser" :lisp-dir
	     "elisp")
     (vue-mode :url "https://github.com/AdamNiederer/vue-mode"
	       :shell-command "cask install")
     (ibuffer-vc :url "https://github.com/purcell/ibuffer-vc" :make
		 "compile")
     (diredfl :url "https://github.com/purcell/diredfl" :make
	      "compile")
     (f :url "https://github.com/rejeep/f.el" :shell-command
	"cask install"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
