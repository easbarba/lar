;;; -*- lexical-binding: t;

;; Turn off some unneeded UI elements
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
;; (global-display-line-numbers-mode 1) ;; turn on line numbers
(load-theme 'modus-vivendi-tritanopia t)

(electric-pair-mode 1) ;; auto insert closing bracket

;; ** ENABLE GLOBAL FEATURES
(global-auto-revert-mode t)
(prefer-coding-system 'utf-8)
(which-function-mode)
(pixel-scroll-mode)

(delete-selection-mode)

(setq visible-bell t
      user-full-name "Euber Alexandre Barbosa"
      user-real-login-name "easbarba"

      display-line-numbers-type nil
      org-directory "~/Documents/org"

      ;; max-lisp-eval-depth 20600
      network-security-level 'high
      read-process-output-max (* 1024 1024)
      global-completion-preview-mode t
      gnutls-min-prime-bits 4096
      max-specpdl-size 1500
      load-prefer-newer t
      ;; debug-on-error t
      global-subword-mode t
      history-delete-duplicates t
      history-length 1000
      blink-matching-paren 'jump-offscreen
      default-directory "~/"
      text-quoting-style 'grave
      pixel-dead-time 0
      pixel-resolution-fine-flag t
      mouse-autoselect-window t
      focus-follows-mouse t
      scroll-conservatively 100000
      apropos-sort-by-scores t
      register-separator ?+
      use-short-answers t
      initial-scratch-message ""
      browse-url-generic-program "firefox"
      browse-url-browser-function 'browse-url-generic
      use-package-always-ensure t
      inhibit-startup-message t)

;; ===============  FONTS

(if (member "JetBrains Mono" (font-family-list))
    (add-to-list 'default-frame-alist
		 '(font . "JetBrains Mono-16"))

  (when (string-equal (system-name) "easbarba-x220")
    (add-to-list 'default-frame-alist
		 '(font . "DejaVu Sans Mono-15")))

  (add-to-list 'default-frame-alist
	       '(font . "DejaVu Sans Mono-17")))

;; ========================= MACROS

(defmacro assoc-val (var vars)
  "Get value(cdr) of alist."
  `(cdr (assoc ',var ,vars)))

(defmacro assoc-key (var vars)
  "Get key(car) of alist."
  `(car (assoc ',var ,vars)))

(defun pack-installed-p (package)
  "Predicate: Confirm if PACKAGE is installed."
  (when (executable-find package)
    t))

(defun e/return-exec (apps)
  "Return first executable in APPS found. TODO: return if running"
  (require 'cl-lib)
  (cl-dolist (app apps)
    (when (executable-find app)
      (cl-return app))))
