;;; -*- lexical-binding: t;

(use-package vue-mode
  :after eglot
  :vc (:url "https://github.com/AdamNiederer/vue-mode" :shell-command "cask install")
  ;; :preface
  ;; (define-derived-mode vue-mode web-mode "Vue")
  ;; (add-to-list 'auto-mode-alist '("\\.vue\\'" . vue-mode))
  :custom  (eglot-connect-timeout 120)
  :preface
  (defun vue-eglot-init-options ()
    (let ((tsdk-path (file-name-concat "/home"
  				       user-login-name
  				       ".config"
  				       "npm"
  				       "lib"
  				       "node_modules"
  				       "typescript"
  				       "lib")))
      `(:typescript (:tsdk ,tsdk-path
  			   :languageFeatures (:completion
  					      (:defaultTagNameCase "both"
  								   :defaultAttrNameCase "kebabCase"
  								   :getDocumentNameCasesRequest nil
  								   :getDocumentSelectionRequest nil)
  					      :diagnostics
  					      (:getDocumentVersionRequest nil))
  			   :documentFeatures (:documentFormatting
  					      (:defaultPrintWidth 100
  								  :getDocumentPrintWidthRequest nil)
  					      :documentSymbol t
  					      :documentColor t)))))
  :config
  (add-to-list 'eglot-server-programs
  	       `(vue-mode . ("vue-language-server" "--stdio" :initializationOptions ,(vue-eglot-init-options))))
  :hook (((vue-ts-mode vue-mode) . eglot-ensure)))

(use-package geiser
  :vc (:url "https://gitlab.com/emacs-geiser/geiser" :lisp-dir "elisp")
  :custom (geiser-repl-history-filename (file-name-concat user-emacs-directory "geiser-history")))

(use-package geiser-guile
  :vc (:url "https://gitlab.com/emacs-geiser/guile")
  :custom
  (geiser-active-implementations '(guile))
  (geiser-guile-binary "guile")
  (geiser-default-implementation 'guile))

(use-package bash-completion
  :vc (:url "https://github.com/szermatt/emacs-bash-completion")
  :config (bash-completion-setup))

(use-package markdown
  :vc (:url "https://github.com/jrblevin/markdown-mode" :make "all")
  :mode (("\\.md\\'" . markdown-mode))
  :custom (markdown-command "multimarkdown"))

(use-package git-modes
  :vc (:url "https://github.com/magit/git-modes"))

(use-package meson-mode
  :vc (:url "https://github.com/wentasah/meson-mode"))

(use-package yaml-mode
  :vc (:url "https://github.com/yoshiki/yaml-mode")
  :hook (yaml-mode . eglot-ensure))

(use-package web-mode
  :vc (:url "https://github.com/fxbois/web-mode")
  :hook (web-mode . eglot-ensure))

(use-package dockerfile-mode
  :vc (:url "https://github.com/spotify/dockerfile-mode")
  :mode(("\\Containerfile\\'" . dockerfile-mode)))

(use-package yasnippet
  :vc (:url "https://github.com/joaotavora/yasnippet")
  :defer t
  :config
  (yas-global-mode 1)
  (yas-reload-all)
  (define-key yas-minor-mode-map [(tab)] nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (global-set-key (kbd "C-c Y") 'yas-insert-snippet)
  (define-key yas-minor-mode-map (kbd "C-c y") yas-maybe-expand)
  :custom
  (yas-wrap-around-region t)
  (yas-triggers-in-field t)
  (yas-prompt-functions '(yas-ido-prompt yas-completing-prompt))
  (yas-verbosity 1)
  (yas-fallback-behavior 'return-nil))

(use-package yasnippet-snippets
  :vc (:url "https://github.com/AndreaCrotti/yasnippet-snippets")
  :after yasnippet
  :config (yasnippet-snippets-initialize))

(use-package apheleia
  :vc (:url "https://github.com/radian-software/apheleia")
  :config (apheleia-global-mode +1))

(use-package aggressive-indent
  :vc (:url "https://github.com/Malabarba/aggressive-indent-mode")
  :custom (aggressive-indent-comments-too t)
  :config
  (define-key aggressive-indent-mode-map (kbd "C-c z") nil)
  (global-aggressive-indent-mode 1))

(use-package git-timemachine
  :vc (:url "https://codeberg.org/pidu/git-timemachine"))

(use-package browse-at-remote
  :vc (:url "https://github.com/rmuslimov/browse-at-remote" :make "compile")
  :bind ("C-c a g" . 'browse-at-remote ))

(use-package vterm
  :disabled
  :vc (:url "https://github.com/iostapyshyn/eshell-vterm")
  :demand t
  :after eshell
  :config (eshell-vterm-mode))

(provide 'nongnu-prog)
