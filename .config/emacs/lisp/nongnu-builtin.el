;;; -*- lexical-binding: t;

;; ===================== MISC

(use-package undo-fu
  :disabled
  :vc (:url "https://codeberg.org/ideasman42/emacs-undo-fu" :make "compile")
  :config
  (global-unset-key (kbd "C-z"))
  (global-set-key (kbd "C-z")   'undo-fu-only-undo)
  (global-set-key (kbd "C-S-z") 'undo-fu-only-redo))

(use-package diredfl
  :vc (:url "https://github.com/purcell/diredfl" :make "compile"))

(use-package ibuffer-vc
  :vc (:url "https://github.com/purcell/ibuffer-vc" :make "compile")
  :config (add-hook 'ibuffer-hook
		    (lambda ()
		      (ibuffer-vc-set-filter-groups-by-vc-root)
		      (unless (eq ibuffer-sorting-mode 'alphabetic)
			(ibuffer-do-sort-by-alphabetic)))))

;; ================= SHELL

(use-package eshell-up
  :vc (:url "https://github.com/peterwvj/eshell-up")
  :after eshell-mode)

(use-package esh-help
  :vc (:url "https://github.com/tom-tan/esh-help")
  :after eshell-mode
  :config (setup-esh-help-eldoc))

(use-package eshell-did-you-mean
  :vc (:url "https://github.com/xuchunyang/eshell-did-you-mean")
  :after eshell-mode
  :config (eshell-did-you-mean-setup))

(use-package eshell-syntax-highlighting
  :vc (:url "https://github.com/akreisher/eshell-syntax-highlighting")
  :after eshell-mode
  :config (eshell-syntax-highlighting-global-mode +1))

(use-package eshell-toggle
  :vc (:url "https://github.com/4DA/eshell-toggle")
  :bind (("C-c o e" . eshell-toggle)))

;; ================== ORG

(use-package org-modern
  :vc (:url "https://github.com/minad/org-modern")
  :hook (org-mode . 'global-org-modern-mode))

(provide 'nongnu-builtin)
