;;; -*- lexical-binding: t;

(use-package hl-todo
  :vc (:url "https://github.com/tarsius/hl-todo" :make "lisp")
  :hook (prog-mode . 'highlight-indent-guides-mode))

(use-package diff-hl
  :vc (:url "https://github.com/dgutov/diff-hl" :make "compile"))

(use-package deft
  :vc (:url "https://github.com/jrblevin/deft" :make "all"))

(use-package emojify
  :disabled
  :vc (:url "https://github.com/iqbalansari/emacs-emojify" :shell-command "cask install")
  :config (global-emojify-mode))

(use-package ligature
  :disabled
  :vc (:url "https://github.com/mickeynp/ligature.el")
  :config
  (ligature-set-ligatures 'prog-mode '("--" "---" "==" "===" "!=" "!==" "=!="
				       "=:=" "=/=" "<=" ">=" "&&" "&&&" "&=" "++" "+++" "***" ";;" "!!"
				       "??" "???" "?:" "?." "?=" "<:" ":<" ":>" ">:" "<:<" "<>" "<<<" ">>>"
				       "<<" ">>" "||" "-|" "_|_" "|-" "||-" "|=" "||=" "##" "###" "####"
				       "#{" "#[" "]#" "#(" "#?" "#_" "#_(" "#:" "#!" "#=" "^=" "<$>" "<$"
				       "$>" "<+>" "<+" "+>" "<*>" "<*" "*>" "</" "</>" "/>" "<!--" "<#--"
				       "-->" "->" "->>" "<<-" "<-" "<=<" "=<<" "<<=" "<==" "<=>" "<==>"
				       "==>" "=>" "=>>" ">=>" ">>=" ">>-" ">-" "-<" "-<<" ">->" "<-<" "<-|"
				       "<=|" "|=>" "|->" "<->" "<~~" "<~" "<~>" "~~" "~~>" "~>" "~-" "-~"
				       "~@" "[||]" "|]" "[|" "|}" "{|" "[<" ">]" "|>" "<|" "||>" "<||"
				       "|||>" "<|||" "<|>" "..." ".." ".=" "..<" ".?" "::" ":::" ":=" "::="
				       ":?" ":?>" "//" "///" "/*" "*/" "/=" "//=" "/==" "@_" "__" "???"
				       "<:<" ";;;"))
  (global-ligature-mode t))

(use-package highlight-indent-guides
  :vc (:url "https://github.com/DarthFennec/highlight-indent-guides" :make "lisp"))

(use-package git-gutter
  :vc (:url "https://github.com/emacsorphanage/git-gutter")
  :config (global-git-gutter-mode +1))

(use-package mood-line
  :vc (:url "https://gitlab.com/jessieh/mood-line")
  :config (mood-line-mode)
  :custom (mood-line-glyph-alist mood-line-glyphs-fira-code))

(use-package zoom
  :vc (:url "https://github.com/cyrus-and/zoom")
  :config (zoom-mode)
  :custom (zoom-ignored-major-modes '(dired-mode ediff-mode)))

(use-package perspective
  :vc (:url "https://github.com/nex3/perspective-el" :make "compile")
  :bind ("C-x C-b" . persp-list-buffers)         ; or use a nicer switcher, see below
  :custom (persp-mode-prefix-key (kbd "C-c w"))  ; pick your own prefix key here
  :init (persp-mode)
  :config
  (dolist (w '("1" "2" "3" "4"))
    (persp-switch w))
  (persp-switch "1"))

(use-package multiple-cursors
  :vc (:url "https://github.com/magnars/multiple-cursors.el")
  :bind (("C-S-c C-S-c" . 'mc/edit-lines)
	 ("C->" . 'mc/mark-next-like-this)
	 ("C-<" . 'mc/mark-previous-like-this)
	 ("C-c C-<" . 'mc/mark-all-like-this)))


(use-package swiper
  :vc (:url "https://github.com/abo-abo/swiper" :make "compile")
  :config (ivy-mode)
  :custom ((ivy-use-virtual-buffers t)
  	   (enable-recursive-minibuffers t))
  :bind (("\C-s" . 'swiper)
  	 ("C-c C-r" . 'ivy-resume)
  	 ("<f6>" . 'ivy-resume)
  	 ("M-x" . 'counsel-M-x)
  	 ("C-x C-f" . 'counsel-find-file)
  	 ("<f1> f" . 'counsel-describe-function)
  	 ("<f1> v" . 'counsel-describe-variable)
  	 ("<f1> o" . 'counsel-describe-symbol)
  	 ("<f1> l" . 'counsel-find-library)
  	 ("<f2> i" . 'counsel-info-lookup-symbol)
  	 ("<f2> u" . 'counsel-unicode-char)
  	 ("C-c g" . 'counsel-git)
  	 ("C-c j" . 'counsel-git-grep)
  	 ("C-c k" . 'counsel-ag)
  	 ("C-x l" . 'counsel-locate)))

(use-package counsel
  :vc (:url "https://github.com/abo-abo/swiper" :make "compile"))

(use-package ivy
  :vc (:url "https://github.com/abo-abo/swiper" :make "compile"))

(use-package corfu
  :vc (:url "https://github.com/minad/corfu")
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto t)
  (corfu-quit-no-match 'separator) ;; or t
  (corfu-echo-documentation t)
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))
  :config
  (global-corfu-mode)

  (use-package emacs
    :custom
    ;; TAB cycle if there are only few candidates
    ;; (completion-cycle-threshold 3)

    ;; Enable indentation+completion using the TAB key.
    ;; `completion-at-point' is often bound to M-TAB.
    (tab-always-indent 'complete)

    ;; Emacs 30 and newer: Disable Ispell completion function.
    ;; Try `cape-dict' as an alternative.
    (text-mode-ispell-word-completion nil)

    ;; Hide commands in M-x which do not apply to the current mode.  Corfu
    ;; commands are hidden, since they are not used via M-x. This setting is
    ;; useful beyond Corfu.
    (read-extended-command-predicate #'command-completion-default-include-p))

  (use-package corfu-popupinfo
    :vc (:url "https://github.com/minad/corfu" :lisp-dir "extensions")
    :config
    ;; (corfu-echo-mode)
    (corfu-popupinfo-mode)
    ;; (corfu-info-mode)
    :custom (corfu-info-documentation t))
  (use-package cape
    :vc (:url "https://github.com/minad/cape")
    :bind ("C-c p" . cape-prefix-map) ;; Alternative key: M-<tab>, M-p, M-+
    ;; Alternatively bind Cape commands individually.
    ;; :bind (("C-c p d" . cape-dabbrev)
    ;;        ("C-c p h" . cape-history)
    ;;        ("C-c p f" . cape-file)
    ;;        ...)
    :config
    ;; Add to the global default value of `completion-at-point-functions' which is
    ;; used by `completion-at-point'.  The order of the functions matters, the
    ;; first function returning a result wins.  Note that the list of buffer-local
    ;; completion functions takes precedence over the global list.
    ;; (add-hook 'completion-at-point-functions #'cape-history)
    (add-hook 'completion-at-point-functions #'cape-dabbrev)
    (add-hook 'completion-at-point-functions #'cape-file)
    (add-hook 'completion-at-point-functions #'cape-elisp-block)))

(provide 'nongnu-ui)
