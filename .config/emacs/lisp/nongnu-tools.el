;;; -*- lexical-binding: t;

;; '( :url :branch :lisp-dir :main-file :vc-backend :rev :shell-command :make :ignored-files)

(use-package emms
  :vc (:url "https://git.savannah.gnu.org/git/emms.git")
  :defer t
  :config
  (require 'emms-setup)
  (emms-all)
  :custom
  (emms-player-list '(emms-player-mpv))
  (emms-source-file-default-directory (file-name-concat *home* "Music"))
  :bind (:map emms-playlist-mode-map
	      ("l" . #'emms-toggle-repeat-playlist)
	      ("p" . #'emms-insert-playlist)
	      ("i" . #'emms-insert-file)
	      ("t" . #'emms-toggle-repeat-track)
	      ("s" . #'emms-playlist-save)
	      ("m" . #'emms-shuffle)))

(use-package yeetube
  :vc (:url "https://codeberg.org/ThanosApollo/emacs-yeetube")
  :config (define-prefix-command 'my/yeetube-map)
  :bind (("C-c y" . 'my/yeetube-map)
	 :map my/yeetube-map
	 ("s" . 'yeetube-search)
	 ("b" . 'yeetube-play-saved-video)
	 ("d" . 'yeetube-download-videos)
	 ("p" . 'yeetube-mpv-toggle-pause)
	 ("v" . 'yeetube-mpv-toggle-video)
	 ("V" . 'yeetube-mpv-toggle-no-video-flag)
	 ("k" . 'yeetube-remove-saved-video)))

(use-package elfeed
  :vc (:url "https://github.com/skeeto/elfeed")
  :custom ((elfeed-db-directory "~/.config/elfeed")
	   (elfeed-feeds
	    '("https://www.fsf.org/static/fsforg/rss/news.xml"
	      "https://www.fsf.org/static/fsforg/rss/blogs.xml"
	      ;; scheme blogs
	      "https://wingolog.org/feed/atom"
	      "https://elephly.net/feed.xml"
	      "https://guix.gnu.org/feeds/blog.atom"
	      "https://dthompson.us/feed.xml"
	      ;; misc blogs
	      "https://tonarinoyj.jp/rss/series/13932016480028984490"
	      ;; distros blogs
	      "https://guix.gnu.org/feeds/blog.atom"
	      "https://pointieststick.com/feed"
	      ;; prog blogs
	      "https://michael.stapelberg.ch/feed.xml"
	      "https://edelpero.svbtle.com/feed"
	      "https://drewdevault.com/blog/index.xml"
	      "https://ambrevar.xyz/atom.xml"
	      "https://unixsheikh.com/feed.rss"
	      "https://sourcehut.org/blog/index.xml"
	      "https://nullprogram.com/feed/"
	      "https://billykorando.com/feed/"
	      ;; progs blogs br
	      "https://leandronsp.com/rss.xml"
	      ;; devops
	      "https://blog.bobbyallen.me/feed/"
	      ;; devops br
	      "https://feeds.feedburner.com/ramonduraes"
	      "https://knela.dev/index-rally?format=rss"
	      ;; ===========
	      ;; CANAIS
	      ;; ===========
	      ;; SQL
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCAWsBMQY4KSuOuGODki-l7A" ;; Data interview Pro
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCiWbFL7I1PgC5OnhIVkXySQ" ;; Vanny
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCW8Ews7tdKKkBT6GdtQaXvQ" ;; StrataScratch
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCCDA5Yte0itW_Bf6UHpbHug" ;; Postgres Open
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCsJkVvxwoM7R9oRbzvUhbPQ" ;; Postgres Conference
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCkIPoYyNr1OHgTo0KwE9HJw" ;; EDB
	      ;; PROGRAMACAO GERAL
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC2KfmYEM4KCuA1ZurravgYw" ;; Amigos Code
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCqa6i-EYjkIqVhu1CpsBOPQ" ;; Filho da nuvem
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@TravisMedia" ;; Travis Media
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC9-y-6csu5WGm29I7JiwpnA" ;; Computerphile
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@JacobSorber" ;; Jacob Sorber
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@LowLevelTV" ;; LowLevelTV
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@GOTO-" ;; Goto; Conf
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@ByteByteGo" ;; ByteByteGo
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@Fireship" ;; Fireship
	      ;; GNU/LINUX/FLOSS
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCuj_loxODrOPxSsXDfJmpng" ;; Andrew Tropin
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC9NuJImUbaSNKiwF2bdSfAw" ;; FOSDEM
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC629vKGFPRc1rz6VDm6OZiQ" ;; Diolinux clips
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC5UAwBUum7CPN5buc-_N1Fw" ;; Linux Experiment
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg" ;; Distro Tube
	      ;; DEVOPS
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@VeronicaExplains" ;; Veronica Explains
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCfz8x0lVzJpb_dgWm9kPVrw" ;; DevOps Toolkit
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCR-DXc1voovS8nhAvccRZhg" ;; Jeff Geerling
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCCCuGbB3Vf6AKags3WgW2Cw" ;; souzaxx
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCQnpN5AUd36lnMHuIl_rihA" ;; Caio Delgado
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC3yoPZJdjwOjrHWy_iEP08A" ;; Igor Souza
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC-JJ1NXjx8QI-tuG0PMFbhA" ;; Matheus Fidelis
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCdngmbVKX1Tgre699-XLlUA" ;; TechWorld with Nana
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCfz8x0lVzJpb_dgWm9kPVrw" ;; DevOps Toolkit
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UConIMoZxnroqKqCc-utZA7A" ;; Carlos e Nogueira
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCPNrIITPNFFLmcU3VfoKuGQ" ;; Gomex
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCJnKVGmXRXrH49Tvrx5X0Sw" ;; Linuxtips br
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCyNp3i0UZeTL11CUBs9mZyA" ;; Punkdevops
	      ;; GERAIS
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCqRraVICLr0asn90cAvkIZQ" ;; Corinthians TV
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@meutimao" ;; MeuTimao
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@GetBeamed" ;; Get Bearned
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCmRFj7s3qBtadUujBd3Tujg" ;; Panelaco
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UC0fGGprihDIlQ3ykWvcb9hg" ;; Tese onze
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCQNp5qrzckO45QFyaVPh9Lg" ;; Ju Furno
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCW3nde-8K-5BaHAmQLZ7ycg" ;; Silvio Almeida
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCdKJlY5eAoSumIlcOcYxIGg" ;; Nunca Vi 1 cientista
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCrSM1UV_KLAdTzgxyxl3POw" ;; Ola Ciencia
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@miracleprocess" ;; Miracle Process
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@jonathanbynoe" ;; Jonathan Bynoe
	      ;; LINGUAGENS
	      "https://www.youtube.com/feeds/videos.xml?channel_id=@LalaChinese" ;; Lala Chinese
	      ;; VEGAN
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCs1uwp7bB1J_3r5xN2ioL_w" ;; High Carb Hannah
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCtdrvQPKPB7dQG5XJsbillQ" ;; Flavio Giusti
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCZ6JHFBaDUJ9wfo41HSII_w" ;; Fabio Chaves
	      "https://www.youtube.com/feeds/videos.xml?channel_id=UCEjkioV3LO_OIUaSWRxFZ3A" ;; Cheap Lazy Vegan
	      ;; EMACS
	      "http://sachachua.com/blog/category/emacs/feed"))
	   (url-queue-timeout 30)
	   (elfeed-search-filter "@2-week-ago +unread"))
  :config
  ;; Entries older than 2 weeks are marked as read
  (add-hook 'elfeed-new-entry-hook
	    (elfeed-make-tagger :before "3 weeks ago"
				:remove 'unread))

  (defun e/elfeed-yt-channel-id ()
    "Paste Youtube's channel id to formatted RSS feed for Elfeed or GNUS."
    (interactive)
    (insert (concat "\""
		    "https://www.youtube.com/feeds/videos.xml?channel_id="
		    (nth 4
			 (split-string
			  (read-from-minibuffer
			   "Enter Youtube's channel id: ") "/"))
		    "\" ;; "
		    (read-from-minibuffer "Enter Elfeed entry name: "))))

  (defun e/elfeed-play-with-video-player ()
    "Play entry link with video player - Ambrevar."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode)
		     elfeed-show-entry
		   (elfeed-search-selected :single))))
      (e/play-video (elfeed-entry-link entry))
      (elfeed-search-untag-all (elfeed-entry-link entry))))
  (define-key elfeed-search-mode-map "m" #'e/elfeed-play-with-video-player))

(use-package move-text
  :vc (:url "https://github.com/emacsfodder/move-text"))

(use-package pdf-tools
  :vc (:url "https://github.com/vedang/pdf-tools" :lisp-dir "lisp" :shell-command "cask install")
  :config
  (pdf-tools-install)
  ;; (add-hook 'pdf-tools-enabled-hook 'auto-revert-mode)
  ;; (add-hook 'pdf-view-mode-hook (lambda () (cua-mode 0)))
  (setq pdf-annot-activate-created-annotations t
	pdf-view-resize-factor 1.1)
  ;; (define-key pdf-view-mode-map (kbd "C-n") 'pdf-view-next-page-command)
  ;; (define-key pdf-view-mode-map (kbd "C-p") 'pdf-view-previous-page-command)
  ;; (define-key pdf-view-mode-map (kbd "n") 'pdf-view-next-line-or-next-page)
  ;; (define-key pdf-view-mode-map (kbd "p")'pdf-view-previous-line-or-previous-page)
  (define-key pdf-view-mode-map (kbd "/") 'pdf-view-midnight-minor-mode)

  ;; --------------- pdf page number on mode-line
  (define-pdf-cache-function pagelabels)
  (add-hook 'pdf-view-mode-hook
	    (lambda ()
	      (setq-local mode-line-position
			  '(" ("
			    ;; (:eval (nth (1- (pdf-view-current-page))
			    ;;		  (pdf-cache-pagelabels)))
			    ;; "/"
			    (:eval (number-to-string
				    (pdf-view-current-page)))
			    "/"
			    (:eval (number-to-string
				    (pdf-cache-number-of-pages)))")"))))

  ;; --------------- pdf-tools reopen last page
  ;; https://github.com/politza/pdf-tools/issues/18#issuecomment-269515117

  (defun e/pdf-set-last-viewed-bookmark ()
    (interactive)
    (when (eq major-mode 'pdf-view-mode)
      (bookmark-set (e/pdf-generate-bookmark-name))))

  (defun e/pdf-jump-last-viewed-bookmark ()
    (bookmark-set "fake") ; this is new
    (when
	(e/pdf-has-last-viewed-bookmark)
      (bookmark-jump (e/pdf-generate-bookmark-name))))

  (defun e/pdf-has-last-viewed-bookmark ()
    (assoc
     (e/pdf-generate-bookmark-name) bookmark-alist))

  (defun e/pdf-generate-bookmark-name ()
    (concat "PDF-LAST-VIEWED: " (buffer-file-name)))

  (defun e/pdf-set-all-last-viewed-bookmarks ()
    (dolist (buf (buffer-list))
      (with-current-buffer buf
	(e/pdf-set-last-viewed-bookmark))))
  (add-hook 'kill-buffer-hook 'e/pdf-set-last-viewed-bookmark)
  (add-hook 'pdf-view-mode-hook 'e/pdf-jump-last-viewed-bookmark)
  (unless noninteractive
    (add-hook 'kill-emacs-hook #'e/pdf-set-all-last-viewed-bookmarks)))

(use-package olivetti
  :vc (:url "https://github.com/rnkn/olivetti"))

(use-package magit
  ;; :vc (:url "https://github.com/magit/magit" :lisp-dir "lisp" :make "all")
  :bind ("C-c m" . magit-status)
  :custom (ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package makefile-executor
  :vc (:url "https://github.com/Olivia5k/makefile-executor.el"))

(use-package define-word
  :vc (:url "https://github.com/abo-abo/define-word")
  :bind (("C-c a d p" . define-word-at-point)
	 ("C-c a d w" . define-word)))

(use-package duplicate-thing
  :vc (:url "https://github.com/ongaeshi/duplicate-thing")
  :bind (("C-c a q" . duplicate-thing)))

(use-package ace-window
  :vc (:url "https://github.com/abo-abo/ace-window")
  :bind ("C-x o" . 'ace-window))

(use-package anzu
  :vc (:url "https://github.com/emacsorphanage/anzu")
  :custom (anzu-mode-lighter ""))

(use-package crux
  :vc (:url "https://github.com/bbatsov/crux"))

(use-package async
  :vc (:url "https://github.com/jwiegley/emacs-async")
  :after dired
  :bind (([remap dired-do-copy]          . 'dired-async-do-copy)
	 ([remap dired-do-rename]        . 'dired-async-do-rename)
	 ([remap dired-do-shell-command] . 'dired-do-async-shell-command)
	 ([remap dired-do-symlink]       . 'dired-async-do-symlink)
	 ([remap dired-do-hardlink]      . 'dired-async-do-hardlink)))

(use-package nov
  ;; :vc (:url "https://depp.brause.cc/nov.el.git" )
  :mode ("\\.epub\\'" . nov-mode)
  :hook (nov-mode . olivetti-mode)
  :bind (:map nov-mode-map ("w" . 'define-word-at-point)))

(use-package helpful
  :disabled
  :vc (:url "https://github.com/Wilfred/helpful" :shell-command "cask install")
  :bind (("C-h f" . 'helpful-callable)
	 ("C-h F" . 'helpful-function)
	 ("C-h v" . 'helpful-variable)
	 ("C-h k" . 'helpful-key)
	 ("C-c C-d" . 'helpful-at-point)
	 ("C-c c" . 'helpful-command)))

(use-package casual
  :disabled
  :vc (:url "https://github.com/kickingvegas/casual" :lisp-dir "lisp" :branch "main"))

(use-package dirvish
  :disabled
  :vc (:url "https://github.com/alexluigit/dirvish")
  :config (dirvish-override-dired-mode))

(use-package alert
  :disabled
  :vc (:url "https://github.com/jwiegley/alert" :main-file "alert.el")
  :custom (alert-default-style 'notifications))

(use-package outshine
  :disabled
  :vc (:url "https://github.com/alphapapa/outshine")
  :hook (prog-mode . outshine-mode)
  :bind ("C-c b c" . outshine-cycle))


(provide 'nongnu-tools)
