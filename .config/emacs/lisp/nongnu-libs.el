;;; -*- lexical-binding: t;

(use-package f
  :vc (:url "https://github.com/rejeep/f.el" :shell-command "cask install"))

(use-package s
  :vc (:url "https://github.com/magnars/s.el" :shell-command "cask install"))

(use-package dash
  :vc (:url "https://github.com/magnars/dash.el" :shell-command "cask install"))

(provide 'nongnu-libs)
