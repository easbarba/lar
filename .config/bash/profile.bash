[[ -f $HOME/.profile ]] && source $HOME/.profile
export $(/usr/lib/systemd/user-environment-generators/30-systemd-environment-d-generator | xargs)

# Start Up SSH AGENT
sshkeys() {
	[[ ! -x $(command -v ssh-agent) ]] && return
	[[ ! -x $(command -v ssh-add) ]] && return

	# kill any ssh-agent running, re-enter new one
	# before starting session and reset ssh env vars
	killall ssh-agent

	# if [ -z "$SSH_AGENT_PID" ]; then
	eval "$(ssh-agent -s)"
	# fi

	ssh-add -q "$HOME/.ssh/id_ed25519" </dev/null

	export SSH_AGENT_PID=$(pgrep ssh-agent)
	export SSH_AUTH_SOCK=$(find /tmp/ssh-* -name agent.*)
}

wm() {
    #    exec /usr/lib/plasma-dbus-run-session-if-needed
    #    /usr/bin/startplasma-wayland
    # dbus-run-session

    [[ -x $(command -v sway) ]] && exec sway
    [[ -x $(command -v niri) ]] && exec niri
    [[ -x $(command -v river) ]] && exec river
    [[ -x $(command -v wayfire) ]] && exec wayfire
    [[ -x $(command -v miracle) ]] && exec miracle
}

# WAYLAND
if [ -z "$WAYLAND_DISPLAY" ] && [ "$XDG_VTNR" -eq 3 ]; then # [ "$(tty)" = "/dev/tty3" ]
    sshkeys
    wm
fi
